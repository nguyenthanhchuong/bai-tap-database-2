from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    image = models.ImageField(upload_to='image/',blank=True,null=True)
    publish_date = models.DateTimeField(auto_now=False, auto_now_add = False, null =True, blank=True)


